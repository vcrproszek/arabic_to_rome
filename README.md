##Roman number generator from arabic.
The function is limited to an arabic range 1-3999.

Run generator by, where Y is number from above range:
```
node index.js Y

```

##Tests
There is simple jasmine spec test.
To run tests install jasmine by npm (or yarn) first

```
npm install

```

and then
```
npm test

```
