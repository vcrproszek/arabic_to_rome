var romanNumeralGenerator = (function() {

    var romeRecord = {
        1: 'I',
        2: 'II',
        3: 'III',
        4: 'IV',
        5: 'V',
        6: 'VI',
        7: 'VII',
        8: 'VIII',
        9: 'IX',
        10: 'X',
        20: 'XX',
        30: 'XXX',
        40: 'XL',
        50: 'L',
        60: 'LX',
        70: 'LXX',
        80: 'LXXX',
        90: 'XC',
        100: 'C',
        200: 'CC',
        300: 'CCC',
        400: 'CD',
        500: 'D',
        600: 'DC',
        700: 'DCC',
        800: 'DCCC',
        900: 'CM',
        1000: 'M',
        1500: 'MD',
        2000: 'MM',
        3000: 'MMM'
    };

    var multiplyIndex = [1, 10, 100, 1000];

    var toRomeNumber = function (number) {

        return convertNumberToArray(number).map(function (value) {

            return romeRecord[value];
        }).join('');
    };

    var convertNumberToArray = function (number) {
        var stringNumber = number.toString();
        var numberToArray = [];

        for (var i = 0; i < stringNumber.length; i += 1) {
            // + will convert string to int
            numberToArray.push(+stringNumber.charAt(i) * multiplyIndex[stringNumber.length - i - 1]);
        }

        return numberToArray;
    };


    var isValid = function (argument) {
        if (typeof argument !== 'number') throw 'Passed argument is not a number'
        if (argument < 1) throw 'Passed number is less then 1'
        if (argument > 3999) throw 'Passed number is greater then 3999'

        return true;
    };

    return function(number){

        return isValid(number) ? toRomeNumber(number) : null;
    };
})();
// export function as commonJS module
exports.romanNumeralGenerator = romanNumeralGenerator;