var roman = require('./src/roman.js');
var argument = process.argv[2];
if (!argument) {
	argument = new Date().getFullYear();
	console.log('Nor argument found. Will run with argument', argument);
}

try {
    console.log(roman.romanNumeralGenerator(parseInt(argument)));
}

catch(err) {
    console.error(err);

    return err;
}
