var roman = require('../src/roman.js');

describe('Roman number generator', function () {
  it('less then 1', function () {
    expect(function () {roman.romanNumeralGenerator(0)}).toThrow('Passed number is less then 1');

  });
  it('more then 3999', function () {
  	expect(function () {roman.romanNumeralGenerator(4000)}).toThrow('Passed number is greater then 3999');
  });
  it('not a number', function () {
  	expect(function () {roman.romanNumeralGenerator('number')}).toThrow('Passed argument is not a number');
  });
  it('9', function () {
    expect(roman.romanNumeralGenerator(9)).toEqual('IX');
  });
  it('12', function () {
    expect(roman.romanNumeralGenerator(12)).toEqual('XII');
  });
  it('299', function () {
    expect(roman.romanNumeralGenerator(299)).toEqual('CCXCIX');
  });
  it('999', function () {
    expect(roman.romanNumeralGenerator(999)).toEqual('CMXCIX');
  });
  it('3999', function () {
    expect(roman.romanNumeralGenerator(3999)).toEqual('MMMCMXCIX');
  });
});